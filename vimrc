set nocompatible
filetype off

"   set rtp+=~/.vim/bundle/Vundle.vim
"   call vundle#begin()

"   Plugin 'gmarik/Vundle.vim'
"   call vundle#end()
"   filetype plugin indent on

set tabstop=4
set softtabstop=4
set shiftwidth=4
"	set textwidth=79
set expandtab
"   set autoindent
set fileformat=unix

set number
set laststatus=2
set statusline+=Row:\ %-4l
set statusline+=Col:\ %-4c
set statusline+=TotRows:\ %-4L
set statusline+=File:
set statusline+=%f
syntax on

nnoremap ,test :wa!<CR>:GoTest<CR>
nnoremap ,aaa :-1read ~/.vim/snippets/aaa<CR>jjj
nnoremap ,trun :-1read ~/.vim/snippets/trun<CR>7l
nnoremap ,whit :%s/ \+$//g<CR>
